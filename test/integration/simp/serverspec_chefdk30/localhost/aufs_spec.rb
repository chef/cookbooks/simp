require 'spec_helper'

context 'before bullseye', if: os[:release].to_i < 11 do

  describe package 'aufs-dkms' do
    it { should be_installed }
  end

  describe command 'modinfo aufs' do
    its(:exit_status) { should be_zero }
    its(:stderr) { should be_empty }
    its(:stdout) do
      should match %r{^filename:\s+/lib/modules/.*/updates/dkms/aufs.ko$}
    end
  end
end
