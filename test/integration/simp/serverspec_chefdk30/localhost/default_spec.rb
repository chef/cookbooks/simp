require 'spec_helper'

describe file '/usr/sbin/policy-rc.d' do
  it { should exist }
  it { should be_executable }
end

describe service 'etc.mount' do
  before do
    `systemctl start etc.mount`
  end
  it { should be_running }
end

describe service 'var.mount' do
  before do
    `systemctl start var.mount`
  end
  it { should be_running }
end

### permission maintenance:

# create /var/.permission_info:
describe command 'permission-info /var' do
  its(:exit_status) { should be_zero }
  its(:stdout) { should be_empty }
  its(:stderr) { should be_empty }
end

describe file '/var/.permission_info' do
  it { should exist }
  its(:content) { should include 'd local root:staff 2775' }
end

describe command 'fix-permissions /var' do
  before :all do
    # give the script something to fix:
    File.chown(3, 4, '/var/mail')
    File.chmod(0o1234, '/var/local')
  end

  its(:exit_status) { should be_zero }
  its(:stdout) do
    should include "mode of '/var/local' changed from 1234 (-w--wxr-T)"\
                   ' to 2775 (rwxrwsr-x)'
  end
  its(:stdout) do
    should include "changed ownership of '/var/mail' from sys:adm to root:mail"
  end
  its(:stderr) { should be_empty }
end
