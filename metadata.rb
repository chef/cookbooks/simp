name             'simp'
maintainer       'GSI Helmholtzzentrum fuer Schwerionenforschung GmbH'
maintainer_email 'hpc@gsi.de'
license          'GNU Public License 3.0'
description      'System Image Manipulation cookbook'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url       'https://git.gsi.de/chef/cookbooks/simp'
issues_url       'https://git.gsi.de/chef/cookbooks/simp/issues'
chef_version     '>= 13'
supports         'debian'

version          '1.0.0'
