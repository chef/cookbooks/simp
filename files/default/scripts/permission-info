#!/bin/sh
#
# Copyright 2021 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <C.Huhn@gsi.de>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# collect information about files with non-standard permissions
#  in a directory tree and write them to a .permission_info file,
#  tmpfiles.d format
#


if [ -z "$1" ]; then
    echo "No directory specified, please specify a directory" >&2
    exit 1
fi

TMPFILE=$(mktemp)

# list all files that are not rw?r-?r-? root:root:
find "$1" -\! -\( -user root -a -group root -a -perm -644 -o -type l -\) \
     -printf '%y %P %u:%g %m\n' | sort -k2 -t' ' > "$TMPFILE"
mv "$TMPFILE" "$1/.permission_info"
