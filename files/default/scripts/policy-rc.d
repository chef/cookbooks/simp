#!/bin/sh
#
# Copyright 2008-2021 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <C.Huhn@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Service start policy script
#
# see /usr/share/doc/init-system-helpers/README.policy-rc.d.gz
# for details
#

# simple chroot detection:
chrooted() {
    test "$(stat -c "%d/%i" /)" != \
         "$(stat -Lc "%d/%i" /proc/1/root 2>/dev/null)"
}

if chrooted; then
    # don't start services in chroots:
    exit 101
else
    # start the service:
    exit 0
fi
