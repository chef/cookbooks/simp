# `simp` Chef cookbook

This cookbook configures a system image for netbooting.
It is meant to be run by `chef-solo` inside a `chroot`.

`simp` has two companion cookbooks:
1. The `omg` cookbook manages the server setup for managing the OS images
   and handles the creation of the chroots.
2. The `rofl` cookbook tweaks the netboot nodes to be managed by Chef.

## `symp::default` recipe

The `symp::default` recipe sets up configuration to make the netbooted systems
operable with a read-only root filesystem.

1. Create a ramdisk (tmpfs( on `/local`
2. Create mountpoints `/local/etc` and `/local/var`
3. Overlay filesystem for `/etc`:
   Add `/local/etc` as a writable layer on top of `/etc` with AUFS.
4. Overlay filesystem for `/var`:
   Add `/local/var` as a writable layer on top of `/var` with AUFS.
5. Ramdisk on `/media`
6. Additional Tweaks:
   * Empty `/etc/machine-id` → will be generated at startup
   * Tweak `/usr/sbin/policy-rc.d` → inhibit service starts inside chroots
     at install time (systemd has its own chroot detection that prohibits
     service startup in chroots)

## `simp::persistent_var.rb`

Netboot machines do not retain state between reboots since all data is kept
on ramdisks.

Persistent state between reboots can be achieved eg. by injecting a local
partition to `/local/var` before the overlay mount.

This requires some additional steps:
* Outdated cruft in the local `/var` layer may need to be cleaned up
  (eg. package cached from accidental `apt update`s)
* After netboot nodes have been transferred from one image to another
  (eg. up- or downgrades), file permissions beneath `/var` may not match the
  numeric uids in `/etc/passwd` anymore and have to be fixed.
