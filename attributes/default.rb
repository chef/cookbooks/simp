# overlayfs is very sensitive to underlay fs updates
#  therfore we use aufs if possible, which is more robust
default['simp']['unionfs'] =
  (node['platform'] == 'debian' && node['platform_version'].to_i < 11) ?
    :aufs : :overlayfs
