#
# Cookbook Name:: simp
# Recipe:: default
#
# Copyright 2021-2022, GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <C.Huhn@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# add a ramdisk at /local:
systemd_unit 'local.mount' do
  content(
    Unit: {
      Before: %w[systemd-tmpfiles-setup-local.service],
      DefaultDependencies: false
    },
    Mount: {
      What: 'none',
      Where: '/local',
      Type: 'tmpfs'
    }
  )
  action %i[create enable]
  # `systemd-analyze verify ...` fails inside chroots:
  verify !inside_chroot?
end

# this stuff has to exist beneath /local to make the unionfs work
tmpfiles_netboot = <<-LIST
  d /local/etc 0755 root root
  d /local/var 0755 root root
LIST

if node['simp']['unionfs'] == :overlayfs
  # overlayfs needs two more directories
  tmpfiles_netboot =+ <<-LIST
    d /local/.wd_overlay_etc 0700 root root
    d /local/.wd_overlay_var 0700 root root
  LIST

  mount_unit_what = 'overlay'
  mount_unit_type = 'overlay'
elsif node['simp']['unionfs'] == :aufs
  # required for module build:
  package 'linux-headers-amd64'
  package 'aufs-dkms'

  mount_unit_what = 'none'
  mount_unit_type = 'aufs'
end

file '/etc/tmpfiles.d/netboot.conf' do
  content tmpfiles_netboot
end

# run systemd-tmpfiles for /local
#  (inspired by systemd-tmpfiles-setup-dev.service)
systemd_unit 'systemd-tmpfiles-setup-local.service' do
  content(
    Unit: {
      Description: 'Create transient files/dirs  in /local',
      DefaultDependencies: false,
      Conflicts: %w[shutdown.target],
      After: %w[systemd-sysusers.service],
      Before: %w[sysinit.target local-fs-pre.target shutdown.target]
    },
    Service: {
      Type: 'oneshot',
      RemainAfterExit: 'yes',
      ExecStart: '/bin/systemd-tmpfiles --prefix=/local --create --boot',
      SuccessExitStatus: [65, 73]
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# Unionfs mount for /etc:
systemd_unit 'etc.mount' do
  content(
    Unit: {
      After: %w[systemd-tmpfiles-setup-local.service],
      Requires: %w[systemd-tmpfiles-setup-local.service],
      RequiresMountsFor: '/local',
      DefaultDependencies: false
    },
    Install: {
      RequiredBy: %w[local-fs-pre.target]
    },
    Mount: {
      What: mount_unit_what,
      Type: mount_unit_type,
      Options: node['simp']['unionfs'] == :aufs ?
        'dirs=/local/etc=rw:/etc=ro' :
        'lowerdir=/etc,upperdir=/local/etc,workdir=/local/.wd_overlay_etc',
      Where: '/etc'
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# cleanup cruft from /local/var before union mount
systemd_unit 'cleanup-local-var.service' do
  content(
    Unit: {
      Description: '',
      DefaultDependencies: false,
      RequiresMountsFor: '/local/var',
      Before: %w[var.mount local-fs-pre.target]
    },
    Install: { WantedBy: %w[var.mount] },
    Service: {
      Type: 'oneshot',
      ExecStart: '-rm -r /local/var/lib/dpkg /local/var/cache/apt',
      RemainAfterExit: true
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# Unionfs mount for /var:
systemd_unit 'var.mount' do
  content(
    Unit: {
      After: %w[systemd-tmpfiles-setup-local.service],
      Requires: %w[systemd-tmpfiles-setup-local.service],
      RequiresMountsFor: '/local/var',
      DefaultDependencies: false
    },
    Install: {
      RequiredBy: %w[local-fs-pre.target]
    },
    Mount: {
      What: mount_unit_what,
      Type: mount_unit_type,
      Options: node['simp']['unionfs'] == :aufs ?
        'dirs=/local/var=rw:/var=ro' :
        'lowerdir=/var,upperdir=/local/var,workdir=/local/.wd_overlay_var',
      Where: '/var'
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# /media on tmpfs:
systemd_unit 'media.mount' do
  content(
    # Unit: {},
    Mount: {
      What: 'none',
      Where: '/media',
      Type: 'tmpfs'
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# create initial directories in ramdisk /media
systemd_unit 'fill-media.service' do
  content(
    Unit: {
      Description: 'Create initial directories in /media',
      DefaultDependencies: false,
      RequiresMountsFor: '/media',
      Before: %w[local-fs.target] # ???
    },
    Install: { WantedBy: %w[sysinit.target] },
    Service: {
      Type: 'oneshot',
      ExecStart: '/bin/mkdir -p /media/cdrom0 /media/floppy',
      RemainAfterExit: true
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# tell invoke-rc.d to never restart services in chroots:
cookbook_file '/usr/sbin/policy-rc.d' do
  source 'scripts/policy-rc.d'
  mode 0o755
end

# clear machine-id, it will be regenerated at boot
file '/etc/machine-id' do
  content ''
  mode 0o444
end

# script to collect permission info
#  permissions may be wrong when clients switch system images
#  (numeric uids may differ)
# netboot nodes can use the data generated by this script
#  to fix the permissions at boot time
cookbook_file '/usr/local/sbin/permission-info' do
  source 'scripts/permission-info'
  mode 0o755
end

file '/etc/apt/apt.conf.d/66permission-info' do
  content "DPkg::Post-Invoke {\"/usr/local/sbin/permission-info /var\";};\n"
end

cookbook_file '/usr/local/sbin/fix-permissions' do
  source 'scripts/fix-permissions'
  mode 0o755
end

# fix permissions of files in /var
systemd_unit 'fix-permissions-in-var.service' do
  content(
    Unit: {
      Description: 'Fix permissions of files in /var',
      DefaultDependencies: false,
      RequiresMountsFor: '/var',
      Before: %w[local-fs.target] # ???
    },
    Install: { WantedBy: %w[sysinit.target] },
    Service: {
      Type: 'oneshot',
      ExecStart: '/usr/local/sbin/fix-permissions /var',
      RemainAfterExit: true
    }
  )
  action %i[create enable]
  verify !inside_chroot?
end

# systemd-networkd-wait-online.service does not correctly deduce
#  that the network is already up and blocks the boot
#  until its timeout is reached therefore the service is disabled:
systemd_unit 'systemd-networkd-wait-online.service' do
  action :disable
end
