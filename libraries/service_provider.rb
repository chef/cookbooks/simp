#
# Cookbook Name:: simp
# File:: libraries/service_provider.rb
#
# Copyright 2021-2022 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <C.Huhn@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# override the default service provider so
#

# default to no-op for service[..] resources:
Chef::Resource::Service.default_action = %i[nothing]

# use systemd provider but turn :start and :restart into no-ops
class Chef
  class Provider
    class Service
      # dummy
      class Netboot < Chef::Provider::Service::Systemd
        def start_service
          Chef::Log.info("Stubbing systemctl start #{new_resource}")
        end

        def restart_service
          Chef::Log.info("Stubbing systemctl restart #{new_resource}")
        end
      end
    end
  end
end

# monkey-patch Chef::Resource::Service to hook in our provider unconditionally
class Chef
  class Resource
    # monkey-patch
    class Service
      def provider(_arg = nil)
        set_or_return(
          :provider,
          Chef::Provider::Service::Netboot,
          kind_of: [Class]
        )
      end
    end
  end
end
